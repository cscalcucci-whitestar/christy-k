import XCTest

import Christy_KTests

var tests = [XCTestCaseEntry]()
tests += Christy_KTests.allTests()
XCTMain(tests)
