//
//  File.swift
//  
//
//  Created by Chris Scalcucci on 3/3/22.
//

import Foundation

public enum CollectionError : Error {
    case badAccess
    case outOfBounds
}
