//
//  AtomicBoolean.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 4/8/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public final class AtomicBoolean {

    private let lock = DispatchSemaphore(value: 1)
    private var value : Bool

    public init(_ initialValue: Bool) {
        self.value = initialValue
    }

    // You need to lock on the value when reading it too since
    // there are no volatile variables in Swift as of today.
    public func get() -> Bool {

        lock.wait()
        defer { lock.signal() }
        return value
    }

    public func set(_ newValue: Bool) {

        lock.wait()
        defer { lock.signal() }
        value = newValue
    }

    @discardableResult
    public func flip() -> Bool {

        lock.wait()
        defer { lock.signal() }
        value = !value
        return value
    }

    /**
        Atomically sets the new value, if the current value equals the expected value.

        - parameter expect: The expected value to compare against
        - parameter newValue: The new value to set to if the comparison succeeds
        - returns: True if successful, false indicates that the actualy value was not equal
            to the expected value
     */
    public func compareAndSet(_ expect: Bool, _ newValue: Bool) -> Bool {
        lock.wait()
        defer { lock.signal() }

        if self.value == expect {
            self.value = newValue
            return true
        } else { return false }
    }
}
