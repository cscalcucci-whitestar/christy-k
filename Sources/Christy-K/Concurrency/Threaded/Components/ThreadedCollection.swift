//
//  ThreadedCollection.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 5/9/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

//  Created by Jeremy Schwartz on 2018-06-17.

public protocol ThreadedCollection {

    associatedtype InternalCollectionType

    /// Returns an unmanaged version of the underlying object.
    var unthreaded: InternalCollectionType { get }

}
