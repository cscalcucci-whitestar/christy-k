//
//  AtomicCounter.swift
//  whitestar-swift
//
//  Created by Chris Scalcucci on 3/12/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public class AtomicCounter {

  private var mutex = pthread_mutex_t()
  private var counter: UInt = 0

  public init() {
    pthread_mutex_init(&mutex, nil)
  }

  deinit {
    pthread_mutex_destroy(&mutex)
  }

  public func incrementAndGet() -> UInt {
    pthread_mutex_lock(&mutex)
    defer {
      pthread_mutex_unlock(&mutex)
    }
    counter += 1
    return counter
  }
}
