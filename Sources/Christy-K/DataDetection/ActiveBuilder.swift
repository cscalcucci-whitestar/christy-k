//
//  ActiveBuilder.swift
//  ActiveLabel
//
//  Created by Pol Quintana on 04/09/16.
//  Copyright © 2016 Optonaut. All rights reserved.
//

#if !os(macOS)
import UIKit

extension String {
    func trim(to maximumCharacters: Int) -> String {
        return "\(self[..<index(startIndex, offsetBy: maximumCharacters)])" + "..."
    }
}

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l <= r
    default:
        return !(rhs < lhs)
    }
}

open class ContextData: NSObject {
    var attributedString: NSAttributedString
    var linkResults: [LinkResult]
    var userInfo: [NSObject: AnyObject]?

    // MARK: Initializers

    init(attributedString: NSAttributedString, linkResults: [LinkResult]) {
        self.attributedString = attributedString
        self.linkResults = linkResults
        super.init()
    }
}

public enum LinkDetectionType {
    case none
    case userHandle
    case hashtag
    case url
    case email
    case textLink
    case phoneNumber
}

public struct LinkResult {
    public let detectionType: LinkDetectionType
    public let range: NSRange
    public let text: String
    public let textLink: TextLink?
}

public struct TouchResult {
    public let linkResult: LinkResult?
    public let touches: Set<UITouch>
    public let event: UIEvent?
    public let state: UIGestureRecognizer.State
}

public struct TextLink {
    public let text: String
    public let range: NSRange?
    public let options: NSString.CompareOptions
    public let object: Any?
    public let action: ()->()

    public init(text: String, range: NSRange? = nil, options: NSString.CompareOptions = [], object: Any? = nil, action: @escaping ()->()) {
        self.text = text
        self.range = range
        self.options = options
        self.object = object
        self.action = action
    }
}

open class ContextBuilder {

    let hashtagRegex = "(?<=\\s|^)#(\\w*[a-zA-Z0-9.&_\\-]+\\w*)"
    let userHandleRegex = "(?<=\\s|^)@(\\w*[a-zA-Z0-9.&_\\-]+\\w*)"

    // MARK: - Closures

    public var textFont: ((LinkResult) -> UIFont)?

    public var foregroundHighlightedColor: (LinkResult) -> UIColor? = { (linkResult) in
        switch linkResult.detectionType {
        default:
            return .systemBlue
        }
    }

    public var underlineStyle: (LinkResult) -> NSUnderlineStyle = { (linkResult) in
        switch linkResult.detectionType {
        case .url, .email, .textLink:
            return .single
        default:
            return []
        }
    }

    public var foregroundColor: (LinkResult) -> UIColor = { (linkResult) in
        switch linkResult.detectionType {
        case .userHandle:
            return UIColor(red: 71.0/255.0, green: 90.0/255.0, blue: 109.0/255.0, alpha: 1.0)
        case .hashtag:
            return .red //UIColor(red: 151.0/255.0, green: 154.0/255.0, blue: 158.0/255.0, alpha: 1.0)
        case .url, .email:
            return UIColor(red: 45.0/255.0, green: 113.0/255.0, blue: 178.0/255.0, alpha: 1.0)
        case .textLink:
            return UIColor(red: 45.0/255.0, green: 113.0/255.0, blue: 178.0/255.0, alpha: 1.0)
        case .phoneNumber:
            return UIColor(red: 45.0/255.0, green: 113.0/255.0, blue: 178.0/255.0, alpha: 1.0)
        default:
            return .black
        }
    }

    // linkDetectionTypes
    public var linkDetectionTypes: [LinkDetectionType] = [.userHandle, .hashtag, .url, .textLink, .email, .phoneNumber]

    public var attributedText : NSAttributedString?

    public var text : String = ""

    public var font : UIFont!

    open var textColor: UIColor! {
        didSet {
            _textColor = textColor
        }
    }

    fileprivate var _textColor = UIColor.black

    // Array of link texts
    public var textLinks: [TextLink]? {
        didSet {
            if let textLinks = textLinks {
                if let contextData = contextData {

                  // Add linkResults for textLinks
                  let linkResults = linkResultsForTextLinks(textLinks)
                    contextData.linkResults += linkResults

                  // Addd attributes for textLinkResults
                  let attributedString = addLinkAttributesTo(contextData.attributedString, with: linkResults)
                    contextData.attributedString = attributedString

                  // Set attributedText
                  attributedText = contextData.attributedString
                }
            }
        }
    }

    // Cachable Object to encapsulate all relevant data to restore ContextLabel values
    public var contextData: ContextData? {
        didSet {
            if let contextData = contextData {
                // Set attributedText
                attributedText = contextData.attributedString
            }
        }
    }

    // MARK: - Initializations

    public init(_ font: UIFont, _ textColor: UIColor) {
        self.font = font
        self.textColor = textColor
        setup()
    }

    // MARK: - Methods

    func addAttributes(_ attributes: Dictionary<NSAttributedString.Key, Any>, range: NSRange) {
        if let contextLabelData = contextData {
            let mutableAttributedString = NSMutableAttributedString(attributedString: contextLabelData.attributedString)
            mutableAttributedString.addAttributes(attributes, range: range)

            contextLabelData.attributedString = mutableAttributedString
            attributedText = contextLabelData.attributedString
        }
    }

    open func setContextDataWithText(_ text: String?) {
        var text = text

        if text == nil {
            text = self.text
        }

        if let text = text {
            self.contextData = contextDataWithText(text)
        } else {
            self.contextData = nil
        }
    }

  open func contextDataWithText(_ text: String?) -> ContextData? {
        if let text = text {
            let mutableAttributedString = NSMutableAttributedString(string: text, attributes: attributesFromProperties())

            let _linkResults = linkResults(in: mutableAttributedString)
            let attributedString = addLinkAttributesTo(mutableAttributedString, with: _linkResults)

            return ContextData(attributedString: attributedString, linkResults: _linkResults)
        }
        return nil
    }

    open func setText(_ text: String, withTextLinks textLinks: [TextLink]) {
        self.text = text // calls setContextDataWithText(text)
        self.textLinks = textLinks
    }

    open func attributesFromProperties() -> [NSAttributedString.Key : Any] {
        // Color attributes
        return [
            .font: font as Any,
            .foregroundColor: _textColor
        ]
    }

    fileprivate func attributesWithTextColor(_ textColor: UIColor) -> [NSAttributedString.Key : Any] {
        var attributes = attributesFromProperties()
        attributes[.foregroundColor] = textColor

        return attributes
    }

    fileprivate func attributes(with font: UIFont?, textColor: UIColor, underlineStyle: NSUnderlineStyle) -> [NSAttributedString.Key: Any] {
        var attributes = attributesWithTextColor(textColor)
        attributes[.underlineStyle] = underlineStyle.rawValue
        if let font = font {
            attributes[.font] = font
        }

        return attributes
    }

    fileprivate func setup() {
        // Establish the text store with our current text
        setContextDataWithText(nil)
    }

    // Returns array of link results for all special words, user handles, hashtags and urls
    public func linkResults(in attributedString: NSAttributedString) -> [LinkResult] {
        var linkResults = [LinkResult]()

        if let textLinks = textLinks {
            linkResults += linkResultsForTextLinks(textLinks)
        }

        if linkDetectionTypes.contains(.userHandle) {
            linkResults += linkResultsForUserHandles(inString: attributedString.string)
        }

        if linkDetectionTypes.contains(.hashtag) {
            linkResults += linkResultsForHashtags(inString: attributedString.string)
        }

        if linkDetectionTypes.contains(.url) {
            linkResults += linkResultsForURLs(inAttributedString: attributedString).filter({ $0.detectionType == .url })
        }

        if linkDetectionTypes.contains(.email) {
            linkResults += linkResultsForURLs(inAttributedString: attributedString).filter({ $0.detectionType == .email })
        }

        if linkDetectionTypes.contains(.phoneNumber) {
            linkResults += linkResultsForPhoneNumbers(inAttributedString: attributedString)
        }

        return linkResults
    }

  // TEST: testLinkResultsForTextLinksWithoutEmojis()
  // TEST: testLinkResultsForTextLinksWithEmojis()
  // TEST: testLinkResultsForTextLinksWithMultipleOccuranciesWithoutRange()
  // TEST: testLinkResultsForTextLinksWithMultipleOccuranciesWithRange()
    internal func linkResultsForTextLinks(_ textLinks: [TextLink]) -> [LinkResult] {
        var linkResults: [LinkResult] = []

        for textLink in textLinks {
            let linkType = LinkDetectionType.textLink
            let matchString = textLink.text

            let range = textLink.range ?? NSMakeRange(0, text.count)

            var searchRange = range
            var matchRange = NSRange()
            let nsString = NSString(string: text)
            if nsString.length >= range.location + range.length {
                while matchRange.location != NSNotFound  {
                    matchRange = NSString(string: text).range(of: matchString, options: textLink.options, range: searchRange)
                    if matchRange.location != NSNotFound && (matchRange.location + matchRange.length) <= (range.location + range.length) {
                        linkResults.append(LinkResult(detectionType: linkType, range: matchRange, text: matchString, textLink: textLink))

                        // Remaining searchRange
                        let location = matchRange.location + matchRange.length
                        let length = nsString.length - location
                        searchRange = NSMakeRange(location, length)
                    } else {
                        break
                    }
                }
            }
        }

        return linkResults
    }

    // TEST: testLinkResultsForUserHandlesWithoutEmojis()
    // TEST: testLinkResultsForUserHandlesWithEmojis()
    internal func linkResultsForUserHandles(inString string: String) -> [LinkResult] {
        return linkResults(for: .userHandle, regexPattern: userHandleRegex, string: string)
    }

    // TEST: testLinkResultsForHashtagsWithoutEmojis()
    // TEST: testLinkResultsForHashtagsWithEmojis()
    internal func linkResultsForHashtags(inString string: String) -> [LinkResult] {
        return linkResults(for: .hashtag, regexPattern: hashtagRegex, string: string)
    }

    fileprivate func linkResults(for linkType: LinkDetectionType, regexPattern: String, string: String) -> [LinkResult] {
        guard let regex = try? NSRegularExpression(pattern: regexPattern, options: .caseInsensitive) else {
          return []
        }

        var linkResults: [LinkResult] = []

        // Run the expression and get matches
        let nsString = text as NSString
        let matches = regex.matches(in: text, options: .reportCompletion, range: NSMakeRange(0, nsString.length))

        // Add all our ranges to the result
        for match in matches {
            let matchRange = match.range
            let matchString = NSString(string: text).substring(with: matchRange)

            if matchRange.length > 1 {
                linkResults.append(LinkResult(detectionType: linkType, range: matchRange, text: matchString, textLink: nil))
            }
        }

        return linkResults
    }

    fileprivate func linkResultsForPhoneNumbers(inAttributedString attributedString: NSAttributedString) -> [LinkResult] {
        var linkResults = [LinkResult]()

        // Use a data detector to find phone numbers in the text
        let plainText = attributedString.string

        let dataDetector: NSDataDetector?
        do {
            dataDetector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        } catch _ as NSError {
            dataDetector = nil
        }

        if let dataDetector = dataDetector {
            let matches = dataDetector.matches(in: plainText, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSRange(location: 0, length: plainText.utf16.count))

            // Add a range entry for every phone number we found
            for match in matches {
                let matchRange = match.range
                if let range = plainText.rangeFromNSRange(matchRange) {
                    let phoneNumber = String(plainText[range])
                    linkResults.append(LinkResult(detectionType: .phoneNumber, range: matchRange, text: phoneNumber, textLink: nil))
                }
            }
        }

        return linkResults
    }

    fileprivate func linkResultsForURLs(inAttributedString attributedString: NSAttributedString) -> [LinkResult] {
        var linkResults = [LinkResult]()

        // Use a data detector to find urls in the text
        let plainText = attributedString.string

        let dataDetector: NSDataDetector?
        do {
            dataDetector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        } catch _ as NSError {
            dataDetector = nil
        }

        if let dataDetector = dataDetector {
            let matches = dataDetector.matches(in: plainText, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSRange(location: 0, length: plainText.utf16.count))

            // Add a range entry for every url we found
            for match in matches {
                let matchRange = match.range

                // If there's a link embedded in the attributes, use that instead of the raw text
                var realURL = attributedString.attribute(.link, at: matchRange.location, effectiveRange: nil)

                if realURL == nil {
                    if let range = plainText.rangeFromNSRange(matchRange) {
                        realURL = String(plainText[range])
                    }
                }

                if match.resultType == .link {
                    if let matchString = realURL as? String {
                        if match.url?.scheme == "mailto" {
                            linkResults.append(LinkResult(detectionType: .email, range: matchRange, text: matchString, textLink: nil))
                        } else {
                            linkResults.append(LinkResult(detectionType: .url, range: matchRange, text: matchString, textLink: nil))
                        }
                    }
                }
            }
        }

        return linkResults
    }

    fileprivate func addLinkAttributesTo(_ attributedString: NSAttributedString, with linkResults: [LinkResult], highlighted: Bool = false) -> NSAttributedString {
        let mutableAttributedString = NSMutableAttributedString(attributedString: attributedString)
        for linkResult in linkResults {

            let font = textFont?(linkResult) ?? self.font
            let textColor = foregroundColor(linkResult)
            let highlightedTextColor = foregroundHighlightedColor(linkResult)
            let color = (highlighted) ? highlightedTextColor ?? self.highlightedTextColor(textColor) : textColor
            let underlineStyle = self.underlineStyle(linkResult)
            let attributes = self.attributes(with: font, textColor: color, underlineStyle: underlineStyle)

            mutableAttributedString.setAttributes(attributes, range: linkResult.range)
        }
        return mutableAttributedString
    }

    fileprivate func highlightedTextColor(_ textColor: UIColor) -> UIColor {
        return textColor.withAlphaComponent(0.5)
    }
}

extension String {

    func rangeFromNSRange(_ nsRange : NSRange) -> Range<String.Index>? {
        if let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex) {
            if let to16 = utf16.index(from16, offsetBy: nsRange.length, limitedBy: utf16.endIndex) {
                if let from = String.Index(from16, within: self), let to = String.Index(to16, within: self) {
                    return from ..< to
                }
            }
        }
        return nil
    }

    func NSRangeFromRange(_ range : Range<String.Index>) -> NSRange? {
        let utf16view = self.utf16
        if let from = String.UTF16View.Index(range.lowerBound, within: utf16view),
           let to = String.UTF16View.Index(range.upperBound, within: utf16view) {
            return NSMakeRange(utf16view.distance(from: from, to: from), utf16view.distance(from: from, to: to))
        } else {
            return nil
        }
    }
}
#endif


/*open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
  touchState = .began

  if let linkResult = linkResult(with: touches) {
    selectedLinkResult = linkResult
    let touchResult = TouchResult(linkResult: linkResult, touches: touches, event: event, state: .began)
    if let delegate = delegate {
      delegate.contextLabel(self, didTouchWithTouchResult: touchResult)
    } else {
      didTouch(touchResult)
    }
  } else {
    selectedLinkResult = nil
    let touchResult = TouchResult(linkResult: nil, touches: touches, event: event, state: .began)
    if let delegate = delegate {
      delegate.contextLabel(self, didTouchWithTouchResult: touchResult)
    } else {
      didTouch(touchResult)
    }
  }

  addLinkAttributesToLinkResult(withTouches: touches, highlighted: true)

  super.touchesBegan(touches, with: event)
}*/

/* open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
  touchState = .ended

  addLinkAttributesToLinkResult(withTouches: touches, highlighted: false)

  if let selectedLinkResult = selectedLinkResult {
    let touchResult = TouchResult(linkResult: selectedLinkResult, touches: touches, event: event, state: .ended)
    if let delegate = delegate {
      delegate.contextLabel(self, didTouchWithTouchResult: touchResult)
    } else {
      didTouch(touchResult)
    }
    selectedLinkResult.textLink?.action()
  } else {
    let touchResult = TouchResult(linkResult: nil, touches: touches, event: event, state: .ended)
    if let delegate = delegate {
      delegate.contextLabel(self, didTouchWithTouchResult: touchResult)
    } else {
      didTouch(touchResult)
    }
  }

  selectedLinkResult = nil

  super.touchesEnded(touches, with: event)
}*/


/*
public typealias ConfigureLinkAttribute = (ActiveType, [NSAttributedString.Key : Any], Bool) -> ([NSAttributedString.Key : Any])
public typealias ElementTuple = (range: NSRange, element: ActiveElement, type: ActiveType)
public typealias ActiveFilterPredicate = ((String) -> Bool)

struct ActiveBuilder {

    static func createElements(type: ActiveType, from text: String, range: NSRange, filterPredicate: ActiveFilterPredicate?) -> [ElementTuple] {
        switch type {
        case .mention, .hashtag:
            return createElementsIgnoringFirstCharacter(from: text, for: type, range: range, filterPredicate: filterPredicate)
        case .url:
            return createElements(from: text, for: type, range: range, filterPredicate: filterPredicate)
        case .custom:
            return createElements(from: text, for: type, range: range, minLength: 1, filterPredicate: filterPredicate)
        }
    }

    static func createURLElements(from text: String, range: NSRange, maximumLength: Int?) -> ([ElementTuple], String) {
        let type = ActiveType.url
        var text = text
        let matches = RegexParser.getElements(from: text, with: type.pattern, range: range)
        let nsstring = text as NSString
        var elements: [ElementTuple] = []

        for match in matches where match.range.length > 2 {
            let word = nsstring.substring(with: match.range)
                .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            guard let maxLength = maximumLength, word.count > maxLength else {
                let range = maximumLength == nil ? match.range : (text as NSString).range(of: word)
                let element = ActiveElement.create(with: type, text: word)
                elements.append((range, element, type))
                continue
            }

            let trimmedWord = word.trim(to: maxLength)
            text = text.replacingOccurrences(of: word, with: trimmedWord)

            let newRange = (text as NSString).range(of: trimmedWord)
            let element = ActiveElement.url(original: word, trimmed: trimmedWord)
            elements.append((newRange, element, type))
        }
        return (elements, text)
    }

    private static func createElements(from text: String,
                                            for type: ActiveType,
                                                range: NSRange,
                                                minLength: Int = 2,
                                                filterPredicate: ActiveFilterPredicate?) -> [ElementTuple] {

        let matches = RegexParser.getElements(from: text, with: type.pattern, range: range)
        let nsstring = text as NSString
        var elements: [ElementTuple] = []

        for match in matches where match.range.length > minLength {
            let word = nsstring.substring(with: match.range)
                .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if filterPredicate?(word) ?? true {
                let element = ActiveElement.create(with: type, text: word)
                elements.append((match.range, element, type))
            }
        }
        return elements
    }

    private static func createElementsIgnoringFirstCharacter(from text: String,
                                                                  for type: ActiveType,
                                                                      range: NSRange,
                                                                      filterPredicate: ActiveFilterPredicate?) -> [ElementTuple] {
        let matches = RegexParser.getElements(from: text, with: type.pattern, range: range)
        let nsstring = text as NSString
        var elements: [ElementTuple] = []

        for match in matches where match.range.length > 2 {
            let range = NSRange(location: match.range.location + 1, length: match.range.length - 1)
            var word = nsstring.substring(with: range)
            if word.hasPrefix("@") {
                word.remove(at: word.startIndex)
            }
            else if word.hasPrefix("#") {
                word.remove(at: word.startIndex)
            }

            if filterPredicate?(word) ?? true {
                let element = ActiveElement.create(with: type, text: word)
                elements.append((match.range, element, type))
            }
        }
        return elements
    }
}
*/
