//
//  DataColors.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/23/20.
//  Copyright © 2020 Society. All rights reserved.
//

#if !os(macOS)
import UIKit

public struct DataColors {
    let mention : UIColor = .orange
    let hashtag : UIColor = .green
    let url : UIColor = .systemOrange
    let email : UIColor = .systemBlue
}
#endif
