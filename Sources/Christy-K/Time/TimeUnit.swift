//
//  TimeUnit.swift
//  whitestar-swift
//
//  Created by Chris Scalcucci on 12/12/19.
//  Copyright © 2019 Society. All rights reserved.
//

import Foundation

public enum TimeUnit {
    case nanoseconds
    case microseconds
    case milliseconds
    case seconds
    case minutes
    case hours
    case days
}

