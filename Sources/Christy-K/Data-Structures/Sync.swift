//
//  Sync.swift
//  whitestar-swift
//
//  Created by Chris Scalcucci on 3/3/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

// https://stackoverflow.com/questions/24045895/what-is-the-swift-equivalent-to-objective-cs-synchronized

// Eliminating Lock-Based Code
// https://developer.apple.com/library/archive/documentation/General/Conceptual/ConcurrencyProgrammingGuide/ThreadMigration/ThreadMigration.html#//apple_ref/doc/uid/TP40008091-CH105-SW3

// Mutexes and Closure Capture in Swift
// https://www.cocoawithlove.com/blog/2016/06/02/threads-and-mutexes.html

// NOTE: If we need more performance, move to Matt Gallagher's CwlUtils for PThreadMutex
public func sync(_ lock: Any, _ closure: () -> ()) {
    objc_sync_enter(lock)
    defer { objc_sync_exit(lock) }
    closure()
}

@discardableResult
public func synchronized<T>(_ lock: Any, _ closure: () throws -> T) rethrows -> T {
    objc_sync_enter(lock)
    defer { objc_sync_exit(lock) }
    return try closure()
}

/*
 // Github ZamZamKit

 /// An object that manages the execution of tasks atomically.
 public struct Synchronized<Value> {
     private let mutex = DispatchQueue(label: "\(DispatchQueue.labelPrefix).Synchronized", attributes: .concurrent)
     private var _value: Value

     public init(_ value: Value) {
         self._value = value
     }

     /// Returns or modify the thread-safe value.
     public var value: Value { mutex.sync { _value } }

     /// Submits a block for synchronous, thread-safe execution.
     public mutating func value<T>(execute task: (inout Value) throws -> T) rethrows -> T {
         try mutex.sync(flags: .barrier) { try task(&_value) }
     }
 }
 */

/*
public class PThreadMutex {
   var mutex: pthread_mutex_t

   public init() { /* ... */ }

   public func sync<R>(execute: () throws -> R) rethrows -> R {
      pthread_mutex_lock(&mutex)
      defer { pthread_mutex_unlock(&mutex) }
      return try execute()
   }
}
 */
/*
 // This needs to be copy/pasted into every file it's used, so that Swift can inline the entire function (~10% performance boost)

 private extension PThreadMutex {
    func sync_same_file<R>(execute: () throws -> R) rethrows -> R {
       pthread_mutex_lock(&m)
       defer { pthread_mutex_unlock(&m) }
       return try execute()
    }
 }
 */

// @DaChun
// Use NSLock in Swift4
/*
 Warning The NSLock class uses POSIX threads to implement its locking behavior. When sending an unlock message to an NSLock object, you must be sure that message is sent from the same thread that sent the initial lock message. Unlocking a lock from a different thread can result in undefined behavior.
 */
/*
 let lock = NSLock()
 lock.lock()
 if isRunning == true {
         print("Service IS running ==> please wait")
         return
 } else {
     print("Service not running")
 }
 isRunning = true
 lock.unlock()

 */

// @GNewc
// To add return functionalty, you could do this:
/*
 func synchronize<T>(lockObj: AnyObject!, closure: ()->T) -> T
 {
   objc_sync_enter(lockObj)
   var retVal: T = closure()
   objc_sync_exit(lockObj)
   return retVal
 }

 // Subsequently, you can call it using:
 func importantMethod(...) -> Bool {
   return synchronize(self) {
     if(feelLikeReturningTrue) { return true }
     // do other things
     if(feelLikeReturningTrueNow) { return true }
     // more things
     return whatIFeelLike ? true : false
   }
 }
 */

// @Hanny
// NOTE: Giving up on objc_sync_enter/exit b/c compiler won't allow anything that throws using it, but enter/exit mutex has significant performance gains
/*

 /*
 This code has the re-entry ability and can work with Asynchronous function calls. In this code, after someAsyncFunc() is called, another function closure on the serial queue will process but be blocked by semaphore.wait() until signal() is called. internalQueue.sync shouldn't be used as it will block the main thread if I'm not mistaken.
 */

 let internalQueue = DispatchQueue(label: "serialQueue")
 let semaphore = DispatchSemaphore(value: 1)

 internalQueue.async {

     self.semaphore.wait()

     // Critical section

     someAsyncFunc() {

         // Do some work here

         self.semaphore.signal()
     }
 }
 */

// @Sebastian Boldt
/*
 class MyObject {
     private var internalState: Int = 0
     private let internalQueue: DispatchQueue = DispatchQueue(label:"LockingQueue") // Serial by default

     var state: Int {
         get {
             return internalQueue.sync { internalState }
         }

         set (newState) {
             internalQueue.sync { internalState = newState }
         }
     }
 }
 */

