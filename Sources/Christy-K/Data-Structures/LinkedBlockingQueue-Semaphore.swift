//
//  LinkedBlockingQueue-Semaphore.swift
//  SwiftCollections
//
//  Created by Chris Scalcucci on 7/21/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public enum QueueError : Error {
    case queueFull
}

public final class LinkedBlockingQueue<T: Equatable> {

    /// The capacity bound, or Int.Max if none
    private var capacity : Int

    /// Current number of elements
    private var _count : AtomicInt = AtomicInt(0)

    /// Head of the linked list
    public var head : Node<T>?

    // FIXME: Public for testing only!!!

    /// Tail of linked list
    public var last : Node<T>?  // FIXME: Public for testing only!!!

    /// Lock held by take, poll, etc
    private var takeLock = pthread_mutex_t()

    /// Wait cond for waiting takes
    private var nFilled : Semaphore

    /// Lock held by put, offer, etc
    private var putLock = pthread_mutex_t()

    /// Wait cond for waiting puts
    private var nHoles : Semaphore

    /// Returns the number of elements in this queue
    public var count : Int {
        return _count.get()
    }

    /// Returns the number of additional elements that this queue can
    /// accept without blocking.
    public var remainingCapacity : Int {
        return capacity - count
    }

    public init(_ capacity: Int = Int.max) {
        self.capacity = capacity

        pthread_mutex_init(&takeLock, nil)
        pthread_mutex_init(&putLock, nil)

        nFilled = Semaphore(0)
        nHoles  = Semaphore(capacity)

        head = Node<T>()
        last = head
    }

    public convenience init(_ capacity: Int = Int.max, _ elements: [T]) throws {
        self.init(capacity)

        pthread_mutex_lock(&putLock)

        var n : Int = 0
        var i : Int = 0
        let c : Int = elements.count

        while i < c {
            let e : T = elements[i]
            if n == capacity {
                throw QueueError.queueFull
            }
            enqueue(Node<T>(e))
            n += 1
            i += 1
        }
        _count.set(n)

        defer { pthread_mutex_unlock(&putLock) }
    }

    /**
        Signals a waiting take. Called only from put/offer (which
        do not otherwise ordinarily lock takeLock.)
     */
    private func signalNotEmpty() {
        nHoles.v()
    }

    /**
        Signals a waiting put. Called only from take/poll.
     */
    private func signalNotFull() {
        nFilled.p()
    }

    /**
        Links node at end of queue.

        - parameter node: The node to link
     */
    private func enqueue(_ node: Node<T>) {
        // assert putLock.isHeldByCurrentThread()
        // assert last.next == nil

        //         last = last.next = node;

        last?.next = node
        last = node

        _count.incrementAndGet()
    }

    /**
        Removes a node from head of queue.

        - returns: A node
     */
    private func dequeue() -> T? {
        // assert takeLock.isHeldByCurrentThread()
        // assert head.item == null

        let h = head
        let first = h?.next

        head = first

        let x = first?.item

        first?.item = nil

        _count.decrementAndGet()

        return x
    }

    /**
        Locks to prevent both puts and takes.
     */
    private func fullyLock() {
        pthread_mutex_lock(&putLock)
        pthread_mutex_lock(&takeLock)
    }

    /**
        Unlocks to allow both puts and takes.
     */
    private func fullyUnlock() {
        pthread_mutex_unlock(&takeLock)
        pthread_mutex_unlock(&putLock)
    }

    /**
        Inserts the specified element at the tail of this queue,
        waiting if necessary for space to become available.
     */
    public func put(_ e: T) {

        let node = Node<T>(e)

        nHoles.p()

        // NOTE:- Java uses lockInterruptibly. We could use tryLock?
        pthread_mutex_lock(&putLock)

        enqueue(node)

        pthread_mutex_unlock(&putLock)

        nFilled.v()
    }

    public func take() -> T? {

        var x : T?

        nFilled.p()

        pthread_mutex_lock(&takeLock)

        x = dequeue()

        pthread_mutex_unlock(&takeLock)

        nHoles.v()

        return x
    }

    public func clear() {
        defer {
            fullyUnlock()
        }
        fullyLock()

        var p : Node<T>?
        var h : Node<T>? = head

        while h?.next != nil {
            p = h?.next
            h?.next = h
            p?.item = nil
            h = p
        }

        head = last

        // assert head.item == nil && head.next == nil
        nHoles.reset()
    }

    public func unlink(_ p: Node<T>?, _ pred: Node<T>?) {
        defer {
            fullyUnlock()
        }
        fullyLock()

        p?.item = nil
        pred?.next = p?.next

        if last == p {
            last = pred
        }

        if self._count.getAndDecrement() == capacity {
            signalNotFull()
        }
    }

   /* public func purge(_ fn: (T) -> Bool) {
        defer {
            fullyUnlock()
        }
        fullyLock()

        var pred = head
        var p = pred?.next

        while p != nil {
            if let item = p?.item, fn(item) {
                unlink(p, pred)
            }

            pred = p
            p = p?.next
        }
    }*/

    /**
        Used for any element traversal that is not entirely under lock.

        Suchs traversals must handle both:
            - dequeued nodes (p.next == p)
            - (possibly multiple) interior removed nodes (p.item == nil)
     */
    public func succ(_ p: inout Node<T>?) -> Node<T>? {
        let firstP = p
        p = p?.next
        if firstP == p {
            p = head?.next
        }
        return p
    }

    /*@discardableResult
    private func bulkRemove(_ fn: (T) -> Bool) -> Bool {
        var p : Node<T>?
        var ancestor = head
        var nodes = [Node<T>]()
        var n = 0
        var len = 0

        // 1. Extract batch of up to 64 elements while holding the lock
        fullyLock()

        p = head?.next

        var q = p

        while q != nil {

            if q?.item != nil &&
            q = succ(&q)
        }
    }*/
}

