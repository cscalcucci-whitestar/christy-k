//
//  Map.swift
//  whitestar-swift
//
//  Created by Chris Scalcucci on 1/13/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public protocol MapEntry : Hashable, Comparable {
    associatedtype K : Hashable
    associatedtype V

    func getKey() -> K?
    func getValue() -> V?
    func setValue(_ value: V) -> V?
}

public protocol Map : Hashable {
    associatedtype K : Hashable
    associatedtype V
    associatedtype M : Map
    associatedtype C : Collection
    associatedtype E : MapEntry

    func size() -> Int
    func isEmpty() -> Bool

    func containsKey(_ key: K) -> Bool
    func containsValue(_ value: V) -> Bool

    func get(_ key: K) -> V?
    func put(_ key: K, _ value: V) -> V?
    func remove(_ key: K) -> V?
    func putAll(_ map: M)

    func clear()

    func keySet() -> Set<K>
    func values() -> C
    func entrySet() -> Set<E>

    // func getOrDefault(_ key: K, defaultValue: V) -> V
}

extension Map {
    func getOrDefault(_ key: K, defaultValue: V) -> V {
        return get(key) ?? defaultValue
    }
}
