//
//  LRUCache.swift
//  TubiMovieChallenge
//
//  Created by Christopher Scalcucci on 8/6/19.
//  Copyright © 2019 cscalcucci. All rights reserved.
//

import Foundation
import ChristyKObjCXX

public let DEFAULT_MAXIMUM_LRUCACHE_SIZE = 4096

public class LRUCache<K: Hashable,V: Equatable> {
    public class Node<K: Hashable, V: Equatable> : Hashable, Equatable {
        var key: K
        var value: V

        public init(_ k: K, _ v: V) {
            self.key = k
            self.value = v
        }

        public func hash(into hasher: inout Hasher) {
            hasher.combine(key)
        }

        public static func ==<K: Hashable, V: Equatable>(lhs: Node<K,V>, rhs: Node<K,V>) -> Bool {
            return lhs.key == rhs.key && lhs.value == rhs.value
        }
    }

    public typealias ComputeIfAbsent = (K) -> V?

    // The maximum allowed capacity of the LRU cache
    public private(set) var capacity : Int = DEFAULT_MAXIMUM_LRUCACHE_SIZE

    // A list of recently used nodes, in order of recency
    private var cache = [Node<K,V>]()

    // A mapping of key to value
    private var map = [K:Node<K,V>]()

    // A functor that can be used to fetch a value if a given key contains no value when called
    public var computeIfAbsent : ComputeIfAbsent?

    // Concurrent access control
    private var mutex = SharedRecursiveMutex()

    public var isEmpty : Bool {
        mutex.lock() // Exclusive single-writer access
        defer {
            mutex.unlock()
        }
        return cache.isEmpty
    }

    public var count : Int {
        mutex.lock() // Exclusive single-writer access
        defer {
            mutex.unlock()
        }
        return cache.count
    }

    public init(capacity: Int = DEFAULT_MAXIMUM_LRUCACHE_SIZE) {
        guard capacity > 0 else {
            fatalError("Cannot create LRUCache with a size less than 1.")
        }
        self.capacity = capacity
    }

    public init(_ computeIfAbsent: @escaping ComputeIfAbsent, capacity: Int) {
        guard capacity > 0 else {
            fatalError("Cannot create LRUCache with a size less than 1.")
        }
        self.computeIfAbsent = computeIfAbsent
        self.capacity = capacity
    }

    private func deleteKey(_ k: K) {
        cache.removeFirst(where: { $0.key == k })
        map.removeValue(forKey: k)
    }

    private func makeRecent(_ k: K) {
        guard let node = map[k] else { return }
        deleteKey(k)
        addRecent(k, node)
    }

    private func addRecent(_ k: K, _ node: Node<K,V>) {
        cache.append(node)
        map[k] = node
    }

    private func deleteLeastRecent() {
        guard cache.count > 0 else { return }
        let node = cache.removeFirst()
        map.removeValue(forKey: node.key)
    }

    public func containsKey(_ k: K) -> Bool {
        mutex.lock() // Exclusive single-writer access
        defer {
            mutex.unlock()
        }
        return map.containsKey(k)
    }

    public func removeAll() {
        mutex.lock() // Exclusive single-writer access
        defer {
            mutex.unlock()
        }
        cache.removeAll()
        map.removeAll()
    }

    public func get(_ k: K) -> V? {
        mutex.lock() // Exclusive single-writer access
        defer {
            mutex.unlock()
        }

        if let node = map[k] {
            // Found in cache, so make recent in LRU, then return value
            makeRecent(k)
            return node.value
        } else {
            // didn't find in map, so call computeIfAbsent
            // Cache Miss, need to compute new value
            if let value = computeIfAbsent?(k) {
                put(k, value)
                return value
            }
        }
        return nil
    }

    public func put(_ k: K, _ v: V) {
        mutex.lock() // Exclusive single-writer access
        defer {
            mutex.unlock()
        }

        if let node = map[k] {
            deleteKey(k)
            addRecent(k, node)
        } else {
            if cache.count == capacity {
                deleteLeastRecent()
            }
            addRecent(k, Node(k, v))
        }
    }

    public func putAll(_ src: LRUCache<K,V>) {
        mutex.lock() // Exclusive single-writer access
        defer {
            mutex.unlock()
        }

        src.forEach({
            self.put($0, $1)
        })
    }

    public func remove(_ k: K) -> V? {
        mutex.lock() // Exclusive single-writer access
        defer {
            mutex.unlock()
        }

        // Read any alue without doing a compute if absent
        if let node = map[k] {
            deleteKey(k)
            return node.value
        } else {
            // Key was not in the map
            return nil
        }
    }

    public func forEach(_ fn: @escaping (K,V) -> ()) {
        mutex.lock() // Exclusive single-writer access
        defer {
            mutex.unlock()
        }

        cache.forEach({
            fn($0.key, $0.value)
        })
    }
}


