//
//  Semaphore.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 7/21/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public class Semaphore {
    var count : Int
    private var lock = pthread_mutex_t()
    private var cv = pthread_cond_t()

    public init(_ val: Int) {
        self.count = val
        pthread_mutex_init(&lock, nil)
        pthread_cond_init(&cv, nil)
    }

    /// Acquire & Wait
    public func p() {
        pthread_mutex_lock(&lock)
        while self.count == 0 {
            pthread_cond_wait(&cv, &lock)
        }
        self.count -= 1
        pthread_mutex_unlock(&lock)
    }

    /// Acquire & Signal
    public func v() {
        pthread_mutex_lock(&lock)
        self.count += 1
        if self.count >= 1 {
            pthread_cond_signal(&cv)
        }
        pthread_mutex_unlock(&lock)
    }

    public func reset() {
        pthread_mutex_lock(&lock)
        if self.count >= 1 {
            pthread_cond_signal(&cv)
        }
        pthread_mutex_unlock(&lock)
    }
}

public class Condition {

    var count : Int
    private var waitLock = pthread_mutex_t()
    private var cv = pthread_cond_t()

    public init(_ val: Int) {
        self.count = val
        pthread_mutex_init(&waitLock, nil)
        pthread_cond_init(&cv, nil)
    }

    /// Wait
    public func p() {
        pthread_mutex_lock(&waitLock)
        while self.count == 0 {
            pthread_cond_wait(&cv, &waitLock)
        }
        self.count -= 1
        pthread_mutex_unlock(&waitLock)
    }

    /// Signal
    public func v() {
        self.count += 1
        if self.count >= 1 {
            pthread_cond_signal(&cv)
        }
    }

    public func reset() {
        if self.count >= 1 {
            pthread_cond_signal(&cv)
        }
    }
}
