//
//  File.swift
//  
//
//  Created by Chris Scalcucci on 3/3/22.
//

import Foundation
import ChristyKObjCXX

public protocol ConcurrentObject {

    var mutex : SharedRecursiveMutex { get }

    func sharedReturn<T>(_ fn: () throws -> T) rethrows -> T

    func sharedAction(_ fn: () throws -> Void) rethrows

    func exclusiveReturn<T>(_ fn: () throws -> T) rethrows -> T

    func exclusiveAction(_ fn: () throws -> Void) rethrows
}

public extension ConcurrentObject {

    @inlinable
    func sharedReturn<T>(_ fn: () throws -> T) rethrows -> T {
        defer {
            self.mutex.unlock_shared()
        }
        self.mutex.lock_shared()

        return try fn()
    }

    @inlinable
    func sharedAction(_ fn: () throws -> Void) rethrows {
        defer {
            self.mutex.unlock_shared()
        }
        self.mutex.lock_shared()

        try fn()
    }

    @inlinable
    func exclusiveReturn<T>(_ fn: () throws -> T) rethrows -> T {
        defer {
            self.mutex.unlock()
        }
        self.mutex.lock()

        return try fn()
    }

    @inlinable
    func exclusiveAction(_ fn: () throws -> Void) rethrows {
        defer {
            self.mutex.unlock()
        }
        self.mutex.lock()

        try fn()
    }
}

/*public class ConcurrentObject {

    public var mutex = SharedRecursiveMutex()

    @inlinable
    internal func sharedReturn<T>(_ fn: () throws -> T) rethrows -> T {
        defer {
            self.mutex.unlock_shared()
        }
        self.mutex.lock_shared()

        return try fn()
    }

    @inlinable
    internal func sharedAction(_ fn: () throws -> Void) rethrows {
        defer {
            self.mutex.unlock_shared()
        }
        self.mutex.lock_shared()

        try fn()
    }

    @inlinable
    internal func exclusiveReturn<T>(_ fn: () throws -> T) rethrows -> T {
        defer {
            self.mutex.unlock()
        }
        self.mutex.lock()

        return try fn()
    }

    @inlinable
    internal func exclusiveAction(_ fn: () throws -> Void) rethrows {
        defer {
            self.mutex.unlock()
        }
        self.mutex.lock()

        try fn()
    }

//    public var lock = pthread_rwlock_t()
//
//    public init() {
//        pthread_rwlock_init(&lock, nil)
//    }
//
//    deinit {
//        pthread_rwlock_destroy(&lock)
//    }
//
//    @inlinable
//    internal func sharedReturn<T>(_ fn: () throws -> T) rethrows -> T {
//        defer {
//            pthread_rwlock_unlock(&lock)
//        }
//        pthread_rwlock_rdlock(&lock)
//
//        return try fn()
//    }
//
//    @inlinable
//    internal func sharedAction(_ fn: () throws -> Void) rethrows {
//        defer {
//            pthread_rwlock_unlock(&lock)
//        }
//        pthread_rwlock_rdlock(&lock)
//
//        try fn()
//    }
//
//    @inlinable
//    internal func exclusiveReturn<T>(_ fn: () throws -> T) rethrows -> T {
//        defer {
//            pthread_rwlock_unlock(&lock)
//        }
//        pthread_rwlock_wrlock(&lock)
//
//        return try fn()
//    }
//
//    @inlinable
//    internal func exclusiveAction(_ fn: () throws -> Void) rethrows {
//        defer {
//            pthread_rwlock_unlock(&lock)
//        }
//        pthread_rwlock_wrlock(&lock)
//
//        try fn()
//    }
}
*/
