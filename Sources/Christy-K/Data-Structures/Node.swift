//
//  Node.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 7/21/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public class Node<T: Equatable> : Equatable {
    public var item : T?
    public var next : Node<T>?

    public init(_ item: T? = nil, _ next: Node<T>? = nil) {
        self.item = item
        self.next = next
    }

    public static func ==(lhs: Node<T>, rhs: Node<T>) -> Bool {
        return lhs.item == rhs.item && lhs.next == rhs.next
    }
}
