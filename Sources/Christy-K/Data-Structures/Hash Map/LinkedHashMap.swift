//
//  LinkedDictionary.swift
//  whitestar-swift
//
//  Created by Chris Scalcucci on 1/8/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public class HashNode<K: Hashable, V: Hashable> : Hashable, Equatable, CustomStringConvertible {
    // public var hash : Int
    public var key : K
    public var value : V
    public var next : HashNode<K,V>

    public var description : String {
        return "\(key) = \(value)"
    }

    public init(_ key: K, _ value: V, next: HashNode<K,V> /* hash: Int */) {
        // self.hash = hash
        self.key = key
        self.value = value
        self.next = next
    }

    public func hash(into hasher: inout Hasher) {
        let hash = key.hashValue ^ value.hashValue
        hasher.combine(hash)
    }

    public static func ==(lhs: HashNode<K,V>, rhs: HashNode<K,V>) -> Bool {
        return lhs.key == rhs.key && lhs.value == rhs.value
    }
}

// Inspired by Tim Othyekl
// https://github.com/timothyekl/SwiftDataStructures/blob/master/SwiftDataStructures/OrderedDictionary.swift

// LinkedHashMap behaves like a Dictionary except that it maintains
// the insertion order of the keys, so iteration order matches insertion
// order.
public struct LinkedHashMap<K: Hashable, V: Hashable> {
    public class Entry : HashNode<K,V> {
        public var before, after : Entry?
    }

    private var _dictionary: Dictionary<K,V>
    private var _keys: Array<K>

    /// The head (eldest) of the doubly linked list
    public var head : Entry?
    /// The tail (youngest) of the doubly linked list
    public var tail : Entry?

    /**
        The iteration ordering method for this linked hash map.
        True for access-order, false for insertion-order
     */
    public var accessOrder : Bool!

    // Link at the end of the list
    private mutating func linkNodeLast(_ p: Entry) {
        let last : Entry? = tail
        tail = p
        if last == nil {
            head = p
        } else {
            p.before = last
            last?.after = p
        }
    }

    // Apply src's links to dst
    // TODO in java... b = dst.before = src.before; a = dst.after = src.after
        // Make sure we converted read access properly, i.e. x = y = 10
    private mutating func transferLinks(from src: Entry, to dst: Entry) {
        dst.before = src.before; dst.after = src.after
        let b : Entry? = dst.before
        let a : Entry? = dst.after
        if b == nil {
            head = dst
        } else {
            b?.after = dst
        }
        if a == nil {
            tail = dst
        } else {
            a?.before = dst
        }
    }

    @discardableResult
    public mutating func newNode(_ key: K, _ value: V, next: Entry) -> Entry {
        let p = Entry(key, value, next: next)
        linkNodeLast(p)
        return p
    }

    @discardableResult
    public mutating func replacementNode(p: Entry, next: Entry) -> Entry {
        let q = p
        let t = Entry(q.key, q.value, next: next)
        transferLinks(from: q, to: t)
        return t
    }

    // unlink
    public mutating func afterNodeRemoval(_ e: Entry) {
        let p = e
        let b = p.before
        let a = p.after
        p.before = nil
        p.after = nil
        if b == nil {
            head = a
        } else {
            b?.after = a
        }
        if a == nil {
            tail = b
        } else {
            a?.before = b
        }
    }

    // Possibly remove eldest
    // TODO: Look at java implentation and find out
        // when evict is not true, does removeEldestEntry get called
        // or does the conditional exist out when it fails on evict
    public func afterNodeInsertion(evict: Bool) {
        guard let first : Entry = head else { return }
        if evict && removeEldestEntry(first) {
            let _ = first.key
            //removeNode(
        }
    }



    public var removeEldestEntry : ((Entry) -> Bool) = { _ in return false }

    public init() {
        _dictionary = [:]
        _keys = []
    }

    public init(initialCapacity: Int) {
        _dictionary = Dictionary<K,V>(minimumCapacity: initialCapacity)
        _keys = Array<K>()
    }

    public init(_ dictionary: Dictionary<K,V>) {
        _dictionary = dictionary
        _keys = dictionary.keys.map { $0 }
    }

    public subscript(key: K) -> V? {
        get {
            _dictionary[key]
        }
        set {
            if newValue == nil {
                self.removeValueForKey(key)
            } else {
                _ = self.updateValue(newValue!, forKey: key)
            }
        }
    }

    public mutating func updateValue(_ value: V, forKey key: K) -> V? {
        let oldValue = _dictionary.updateValue(value, forKey: key)
        if oldValue == nil {
            _keys.append(key)
        }
        return oldValue
    }

    public mutating func removeValueForKey(_ key: K) {
        _keys = _keys.filter {
            $0 != key
        }
        _dictionary.removeValue(forKey: key)
    }

    public mutating func removeAll(keepCapacity: Int) {
        _keys = []
        _dictionary = Dictionary<K,V>(minimumCapacity: keepCapacity)
    }

    public var count: Int {
        return _dictionary.count
    }

    // keys isn't lazy evaluated because it's just an array anyway
    public var keys: [K] {
        return _keys
    }

    public var values: Array<V> {
        return _keys.map { _dictionary[$0]! }
    }

    public var description: String {
        var result = "{\n"
        self.enumerated().forEach({ (i,e) in
            let (k,v) = e
            result += "[\(i)]: \(k) => \(v)\n"
        })
        return result + "}"
    }

    public static func ==<K: Equatable, V: Equatable>(lhs: LinkedHashMap<K,V>, rhs: LinkedHashMap<K,V>) -> Bool {
        lhs._keys == rhs._keys && lhs._dictionary == rhs._dictionary
    }

    public static func !=<K: Equatable, V: Equatable>(lhs: LinkedHashMap<K,V>, rhs: LinkedHashMap<K,V>) -> Bool {
        lhs._keys != rhs._keys || lhs._dictionary != rhs._dictionary
    }

}

extension LinkedHashMap {
    // Possibly removes eldest
    // STUBBED kinda 
    public func afterNodeInsertion(_ evict: Bool) {
        if let first : Entry = head {
            if evict && removeEldestEntry(first) {
                var _ : K = first.key // key
                // removeNode(hash(key), key, nil, false, true)
            }
        }
    }
}

extension LinkedHashMap: Sequence {
    public func makeIterator() -> LinkedHashMapIterator<K,V> {
        LinkedHashMapIterator<K,V>(sequence: _dictionary, keys: _keys, current: 0)
    }
}

public struct LinkedHashMapIterator<K: Hashable, V>: IteratorProtocol {
    public let sequence: Dictionary<K,V>
    public let keys: Array<K>
    public var current = 0

    public mutating func next() -> (K,V)? {
        defer { current += 1 }
        guard sequence.count > current else {
            return nil
        }

        let key = keys[current]
        guard let value = sequence[key] else {
            return nil
        }
        return (key, value)
    }
}
