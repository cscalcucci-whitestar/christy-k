//
//  ExpiringCache.swift
//  whitestar-swift
//
//  Created by Chris Scalcucci on 1/8/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public class ExpiringCache {
    public class Entry {
        public var timestamp : Int64
        public var val       : String

        public init(_ val: String, timestamp: Int64) {
            self.timestamp = timestamp
            self.val       = val
        }
    }
    private var millisUntilExpiration : Int!
    private var map : [String:Entry]!

    // Clear out old entries every few queries
    private var queryCount    : Int!
    private var queryOverflow : Int = 300
    private var MAX_ENTRIES   : Int = 200

    public init() {
        expirationInit(3000)
    }

    public init(expiration: Int) {
        expirationInit(expiration)
    }

    // TODO: Add subscripts
    public subscript(key: String) -> Entry? {
        return map[key]
    }

    // TODO: Finish the removeEdlestEntry portion
    private func expirationInit(_ exp: Int) {
        millisUntilExpiration = exp
        map = [String:Entry]()
        queryCount = 0 // TODO
        // OPTIMIZATION
        /*
        map = new LinkedHashMap<>() {
         protected boolean removeEldestEntry(Map.Entry<String,Entry> eldest) {
            return size() > MAX_ENTRIES;
         }
        }
        */
    }

    // OPTIMIZATION synchronized String get(String key) {
    public func get(_ key: String) -> String? {
        if (queryCount + 1) >= queryOverflow {
            cleanup()
        }
        return entry(for: key)?.val ?? nil
    }

    // OPTIMIZATION synchronized void put(String key, String val) {
    public func put(_ key: String, _ val: String) {
        if (queryCount + 1) >= queryOverflow {
            cleanup()
        }
        if let entry = entry(for: key) {
            entry.timestamp = Date.currentTimeMillis()
            entry.val = val
        } else {
            map[key] = Entry(val, timestamp: Date.currentTimeMillis())
        }
    }

    @discardableResult
    private func entry(for key: String) -> Entry? {
        var entry = map[key]
        if entry != nil {
            let delta = Date.currentTimeMillis() - entry!.timestamp
            if delta < 0 || delta >= millisUntilExpiration {
                map.removeValue(forKey: key)
                entry = nil
            }
        }
        return entry
    }

    private func cleanup() {
        map.removeAll()
        queryCount = 0 /*
        // TODO: Find out if the above code is suitable
        // OPTIMIZATION Set<String> keySet = map.KeySet();
        let keys : [String] = map.keyArray
        // Avoid ConcurrentModificationExceptions
        for j in 0..<keys.count {
            entry(for: keys[j])
        } */
    }

    // OPTIMIZED syncrhonized void clear() {
    public func clear() {

    }

}
