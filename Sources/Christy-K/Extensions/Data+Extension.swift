//
//  Data+Extension.swift
//  whitestar-swift
//
//  Created by Chris Scalcucci on 12/11/19.
//  Copyright © 2019 Society. All rights reserved.
//

import Foundation
import Gzip

public extension Data {

    init<T>(from value: T) {
        self = Swift.withUnsafeBytes(of: value) { Data($0) }
    }

    func to<T>(_ type: T.Type) -> T? where T: ExpressibleByIntegerLiteral {
        var value: T = 0
        guard count >= MemoryLayout.size(ofValue: value) else { return nil }
        _ = Swift.withUnsafeMutableBytes(of: &value, { copyBytes(to: $0)} )
        return value
    }
}

public extension Data {
    var hexDescription: String {
        return self.map { String(format: "%02hhx", $0) }.joined()
    }
}

// MARK:- Compression

public extension Data {
    /// Uses GZIP to decompress the data into a string

    /**
        Uses GZIP to decompress the data into a string.

        NOTE: If decompression fails, empty string is returned.

        - returns: The decompressed data as a utf8 String
     */
    func decompressedString() -> String {
        if self.isGzipped {
            return (try? self.gunzipped().utf8String()) ?? ""
        } else {
            return self.utf8String() ?? ""
        }
    }

    /**
        Uses GZIP to decompress the data, if the data is not
        compressed, it will return the initial data instead.
     */
    func decompressed() -> Data {
        if self.isGzipped {
            return (try? self.gunzipped()) ?? self
        } else {
            return self
        }
    }
}

public extension Data {
    init(reading input: InputStream) throws {
        self.init()
        input.open()
        defer {
            input.close()
        }

        let bufferSize = 1024
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: bufferSize)
        defer {
            buffer.deallocate()
        }
        while input.hasBytesAvailable {
            let read = input.read(buffer, maxLength: bufferSize)
            if read < 0 {
                //Stream error occured
                throw input.streamError!
            } else if read == 0 {
                //EOF
                break
            }
            self.append(buffer, count: read)
        }
    }
}


public extension Data {
    func subdata(_ range: ClosedRange<Index>) -> Data {
        return subdata(in: range.lowerBound ..< range.upperBound + 1)
    }
}

public extension Data {
    var string: String? { return String(data: self, encoding: .utf8) }

    func byteArray() -> [UInt8] {
        return [UInt8](self)
    }
}

public extension Data {
    func utf8String() -> String? {
        return String(data: self, encoding: .utf8)
    }
}
