//
//  File.swift
//  
//
//  Created by Chris Scalcucci on 1/18/21.
//

import Foundation

public extension Sequence {
  func reduceInout<A>(_ initial: A, combine: (inout A, Iterator.Element) -> ()) -> A {
    var result = initial
    for element in self {
      combine(&result, element)
    }
    return result
  }
}
