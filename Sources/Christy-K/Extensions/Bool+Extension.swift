//
//  File.swift
//  
//
//  Created by Chris Scalcucci on 1/18/21.
//

import Foundation

public func |= (lhs: inout Bool, rhs: Bool) {
    lhs = lhs || rhs
}
public func &= (lhs: inout Bool, rhs: Bool) {
    lhs = lhs && rhs
}
public func ^= (lhs: inout Bool, rhs: Bool) {
    lhs = lhs != rhs
}

