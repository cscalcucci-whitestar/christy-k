//
//  File.swift
//  
//
//  Created by Chris Scalcucci on 1/18/21.
//

import Foundation

public extension Decimal {
    mutating func round(_ scale: Int, _ roundingMode: NSDecimalNumber.RoundingMode) {
        var localCopy = self
        NSDecimalRound(&self, &localCopy, scale, roundingMode)
    }

    // Non-mutating to protect reference

    func rounded(_ scale: Int, _ roundingMode: NSDecimalNumber.RoundingMode) -> Decimal {
        var result = Decimal()
        var localCopy = self
        NSDecimalRound(&result, &localCopy, scale, roundingMode)
        return result
    }

    func divided(_ divisor: Decimal, _ scale: Int, _ roundingMode: NSDecimalNumber.RoundingMode) -> Decimal {
        var result = Decimal()
        var localCopy = self
        var localDivisor = divisor
        NSDecimalDivide(&result, &localCopy, &localDivisor, roundingMode)
        return result.rounded(scale, roundingMode)
    }

    // Mutating to avoid double references

    mutating func roundedLocal(_ scale: Int, _ roundingMode: NSDecimalNumber.RoundingMode) -> Decimal {
        var result = Decimal()
        NSDecimalRound(&result, &self, scale, roundingMode)
        return result
    }

    mutating func divided(_ divisor: inout Decimal, _ scale: Int, _ roundingMode: NSDecimalNumber.RoundingMode) -> Decimal {
        var result = Decimal()
        NSDecimalDivide(&result, &self, &divisor, roundingMode)
        return result.rounded(scale, roundingMode)
    }
}

public extension Decimal {

    static let one = Decimal(1)

    // MARK:- Common

    var intValue : Int {
        return NSDecimalNumber(decimal: self).intValue
    }

    var doubleValue : Double {
        return NSDecimalNumber(decimal: self).doubleValue
    }

    var floatValue : Float {
        return NSDecimalNumber(decimal: self).floatValue
    }

    var boolValue : Bool {
        return NSDecimalNumber(decimal: self).boolValue
    }

    // MARK:- Signed Integers

    var int8Value : Int8 {
        return NSDecimalNumber(decimal: self).int8Value
    }

    var int16Value : Int16 {
        return NSDecimalNumber(decimal: self).int16Value
    }

    var int32Value : Int32 {
        return NSDecimalNumber(decimal: self).int32Value
    }

    var int64Value : Int64 {
        return NSDecimalNumber(decimal: self).int64Value
    }

    // MARK:- Unsigned Integers

    var uIntValue : UInt {
        return NSDecimalNumber(decimal: self).uintValue
    }

    var uInt8Value : UInt8 {
        return NSDecimalNumber(decimal: self).uint8Value
    }

    var uInt16Value : UInt16 {
        return NSDecimalNumber(decimal: self).uint16Value
    }

    var uInt32Value : UInt32 {
        return NSDecimalNumber(decimal: self).uint32Value
    }

    var uInt64Value : UInt64 {
        return NSDecimalNumber(decimal: self).uint64Value
    }
}
