//
//  Dictionary+Extension.swift
//  whitestar-swift
//
//  Created by Chris Scalcucci on 12/19/19.
//  Copyright © 2019 Society. All rights reserved.
//

import Foundation

public extension Dictionary {
    func containsKey(_ k: Key) -> Bool {
        return self[k] != nil
    }
}

public extension Dictionary {
    func compact<K: Hashable, V>(_ fn: (Key, Value) -> (K,V)?) -> [K:V] {
        var transformed : [K:V] = [:]
        // Iterates the map and only returns values that are not nil from the input fn
        self.forEach({
            if let nv = fn($0.key, $0.value) {
                transformed[nv.0] = nv.1
            }
        })
        return transformed
    }
}

public extension Dictionary {

    /**
        Removes the key (and its corresponding value) from this map.
        This method does nothing if the key is not in the map.

        - parameter key: The key that needs to be removed
        - returns: The previous value associated with key, or nil
            if there was no mapping for key.
     */
    @discardableResult
    mutating func remove(_ key: Key) -> Value? {
        return self.removeValue(forKey: key)
    }

    /**
        Attempts to fetch an element at the target key, if
        there is no present value, runs the provided function
        and associated the generated value with that key

        NOTE: If a value exists, fn does NOT run

        - parameter key: The key to search against
        - parameter fn: A function that generates a value to be placed
        - returns: The generated value, or the original value if found
     */
    @discardableResult
    mutating func computeIfAbsent(_ key: Key, _ fn: () -> (Value)) -> Value {
        if let element = self[key] {
            return element
        } else {
            let element = fn()
            self[key] = element
            return element
        }
    }

}

public extension Dictionary {
    var keyArray : [Self.Key] {
        return Array(self.keys)
    }

    var valueArray : [Self.Value] {
        return Array(self.values)
    }

    /**
        Associates the specified value with the specified key in this map.

        If the map previously contained a mapping for the key, the old value
        is replaced by the specified value.

        - parameter key: Key with which the specified value is to be associated
        - parameter value: Value to be associated with the specified key
        - returns: The previous value if there is one, nil if there was none
     */
    @discardableResult
    mutating func put(_ value: Self.Value, for key: Self.Key) -> Self.Value? {
        let tmp : Self.Value? = self[key]
        self[key] = value
        return tmp
    }
}

public extension Dictionary.Values {
    /// Returns an array of values after having appended an element
    ///
    /// - parameter element: The element to add to the values
    ///
    @discardableResult
     func adding(_ element: Element) -> [Element] {
        var array = Array(self)
        return array.adding(element)
    }
}

public extension Dictionary.Values where Element: Equatable {

    /// Returns the values as an array after having removed all instances of a provided element.
    ///
    @discardableResult
    func removing(_ element: Element) -> [Element] {
        var array = Array(self)
        return array.removing(element)
    }
}

public extension Dictionary {

    func entrySet() -> [[Key:Value]] {
        var tmp : [[Key:Value]] = [[:]]

        self.forEach({ (key,value) in
            tmp.append([key:value])
        })

        return tmp
    }
}

public extension Dictionary {
    func mapValues<T>(_ transform: (Value) throws -> T) rethrows -> Dictionary<Key,T> {
        return Dictionary<Key,T>(uniqueKeysWithValues: zip(self.keys, try self.values.map(transform)))
    }

    func mapKeys<T: Hashable>(_ transform:(Key) throws -> T) rethrows -> Dictionary<T, Value> {
        return Dictionary<T,Value>(uniqueKeysWithValues: zip(try self.keys.map(transform), self.values))
    }
}

public extension Dictionary {
    mutating func putAll(_ map: [Key:Value]) {
        self.merge(map, uniquingKeysWith: { return $1 })
    }
}
