//
//  String+Extensions.swift
//  
//
//  Created by Chris Scalcucci on 1/15/21.
//

import Foundation
import Gzip

public extension StringProtocol {
    subscript(offset: Int) -> Character { self[index(startIndex, offsetBy: offset)] }
    subscript(range: Range<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: ClosedRange<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: PartialRangeFrom<Int>) -> SubSequence { self[index(startIndex, offsetBy: range.lowerBound)...] }
    subscript(range: PartialRangeThrough<Int>) -> SubSequence { self[...index(startIndex, offsetBy: range.upperBound)] }
    subscript(range: PartialRangeUpTo<Int>) -> SubSequence { self[..<index(startIndex, offsetBy: range.upperBound)] }
}

// MARK:- Encoding

public extension String {
    /// Encoding a String to Data using utf8
    var data:          Data  { return Data(utf8) }
    /// Encoding a String to Base64 Data
    var base64Encoded: Data  { return data.base64EncodedData() }
    /// Decoding a Base64 string to Data
    var base64Decoded: Data? { return Data(base64Encoded: self) }

    /// Encoding a String to Base64 String
    var base64EncodedString : String? {
        return self.data.base64EncodedString()
    }

    /// Decoding a Base64 String back into plaintext
    var base64DecodedString : String? {
        var st = self;
        if (self.count % 4 <= 2){
            st += String(repeating: "=", count: (self.count % 4))
        }
        guard let data = Data(base64Encoded: st) else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

// MARK:- Range & Bounding

public extension StringProtocol {
    func ranges(of targetString: Self, options: String.CompareOptions = [], locale: Locale? = nil) -> [Range<String.Index>] {

        let result: [Range<String.Index>] = self.indices.compactMap { startIndex in
            let targetStringEndIndex = index(startIndex, offsetBy: targetString.count, limitedBy: endIndex) ?? endIndex
            return range(of: targetString, options: options, range: startIndex..<targetStringEndIndex, locale: locale)
        }
        return result
    }

    func range(of targetString: Self, options: String.CompareOptions = [], locale: Locale? = nil) -> Range<String.Index>? {
        return ranges(of: targetString).first
    }

    func index(of targetString: Self, options: String.CompareOptions = [], locale: Locale? = nil) -> Int? {
        return range(of: targetString)?.lowerBound.encodedOffset // FIXME TODO Deprecated
    }
}

public extension String {

    subscript(bounds: CountableClosedRange<Int>) -> String {
        let lowerBound = max(0, bounds.lowerBound)
        guard lowerBound < self.count else { return "" }

        let upperBound = min(bounds.upperBound, self.count-1)
        guard upperBound >= 0 else { return "" }

        let i = index(startIndex, offsetBy: lowerBound)
        let j = index(i, offsetBy: upperBound-lowerBound)

        return String(self[i...j])
    }

    subscript(bounds: CountableRange<Int>) -> String {
        let lowerBound = max(0, bounds.lowerBound)
        guard lowerBound < self.count else { return "" }

        let upperBound = min(bounds.upperBound, self.count)
        guard upperBound >= 0 else { return "" }

        let i = index(startIndex, offsetBy: lowerBound)
        let j = index(i, offsetBy: upperBound-lowerBound)

        return String(self[i..<j])
    }

    func substring(_ beginIndex: Int, _ endIndex: Int) -> String {
        return self[beginIndex..<endIndex]
    }

    func substring(_ beginIndex: Int) -> String {
        return String(self.dropFirst(beginIndex))
    }
}

// MARK:- Casting & Conversion

public extension String {
    // Attempt to convert a string into a json dictionary
    func convertToDictionary() -> [String:Any] {
        guard let data = self.data(using: .utf8) else { return [:] }
        do {
            return (try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]) ?? [:]
        } catch {
            print(error.localizedDescription); return [:]
        }
    }
}

public extension String {
    func int() -> Int {
        guard let x : Int = Int(self) else {
            fatalError("Cannot convert String \(self) to an Integer value.")
        }
        return x
    }

    func double() -> Double {
        guard let x : Double = Double(self) else {
            fatalError("Cannot convert String \(self) to a Double value.")
        }
        return x
    }

    func float() -> Float {
        guard let x : Float = Float(self) else {
            fatalError("Cannot convert String \(self) to a Float value.")
        }
        return x
    }

    // e.g. let str  = "{\"name\":\"James\"}"
    //      let dict = str.hashMap()
    //      let name = dict[name] (should be "James")
    func hashMap() -> [String:Any]? {
        guard let data = self.data(using: .utf8) else {
            return nil
        }
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
        } catch {
            // print() instead of non-recoverable error
            fatalError("Casting String to [String:Any]? as JSON failed with \(error.localizedDescription)")
        }
    }
}


// MARK:- Compression

public extension String {
    /// Uses GZIP to compress the string into data

    /**
        Uses GZIP to compress the string into data.

        NOTE: If compression fails, empty data is returned

        - returns: The compressed string in a byte array (Data)
     */
    func compressed() -> Data {
        return (try? self.data.gzipped(level: .bestCompression)) ?? Data()
    }
}

// https://stackoverflow.com/questions/30757193/find-out-if-character-in-string-is-emoji
// Arnold
public extension String {
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
                 0x1F300...0x1F5FF, // Misc Symbols and Pictographs
                 0x1F680...0x1F6FF, // Transport and Map
                 0x2600...0x26FF,   // Misc symbols
                 0x2700...0x27BF,   // Dingbats
                 0xFE00...0xFE0F,   // Variation Selectors
                 0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
                 0x1F1E6...0x1F1FF, // Flags
                 65024...65039,
                 8400...8447,
                 9100...9300,
                 127000...127600:
                 return true
            default:
                continue
            }
        }
        return false
    }
}

public extension Character {
    var containsEmoji : Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
                 0x1F300...0x1F5FF, // Misc Symbols and Pictographs
                 0x1F680...0x1F6FF, // Transport and Map
                 0x2600...0x26FF,   // Misc symbols
                 0x2700...0x27BF,   // Dingbats
                 0xFE00...0xFE0F,   // Variation Selectors
                 0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
                 0x1F1E6...0x1F1FF, // Flags
                 65024...65039,
                 8400...8447,
                 9100...9300,
                 127000...127600:
                return true
            default:
                continue
            }
        }
        return false
    }
}

public extension String {
    private enum StringError : Error {
        case indexOutOfBounds (String)
    }

    func getChars(_ srcBegin: Int, _ srcEnd: Int, _ dst: inout [Character?], _ dstBegin: Int) throws {

        try String.checkBoundsBeginEnd(srcBegin, srcEnd, count)
        try String.checkBoundsOffCount(dstBegin, srcEnd - srcBegin, dst.count)

        try String.getChars(self, srcBegin, srcEnd, &dst, dstBegin)
    }

    static func getChars(_ src: String, _ srcBegin: Int, _ srcEnd: Int, _ dst: inout [Character?], _ dstBegin: Int) throws {

        // Check range
        if (srcBegin < srcEnd) {
           try checkBoundsOffCount(srcBegin, srcEnd - srcBegin, src.count)
        }
        var i = srcBegin
        var x = dstBegin

        while i < srcEnd {

            dst[x] = src[i]
            x += 1
            i += 1
        }
    }

    private static func checkBoundsOffCount(_ offset: Int, _ count: Int, _ length: Int) throws {
        if (offset < 0 || count < 0 || offset > length - count) {
            throw StringError.indexOutOfBounds("offset \(offset), count \(count), length \(length)")
        }
    }

    private static func checkBoundsBeginEnd(_ begin: Int, _ end: Int, _ length: Int) throws {
        if (begin < 0 || begin > end || end > length) {
            throw StringError.indexOutOfBounds("begin \(begin), end \(end), length \(length)")
        }
    }
}

public extension Optional where Wrapped == String {
    var orBlank : String {
        return self ?? ""
    }
}

