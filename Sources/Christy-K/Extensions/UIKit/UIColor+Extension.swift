//
//  UIColor+Extension.swift
//  
//
//  Created by Chris Scalcucci on 1/21/21.
//

#if !os(macOS)
import UIKit

// https://stackoverflow.com/questions/29779128/how-to-make-a-random-color-with-swift - ABakerSmitch

public extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

public extension UIColor {
    static func random() -> UIColor {
        return UIColor(
           red:   .random(),
           green: .random(),
           blue:  .random(),
           alpha: 1.0
        )
    }
}
#endif
