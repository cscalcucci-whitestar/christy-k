//
//  FieldValidation.swift
//  
//
//  Created by Chris Scalcucci on 1/21/21.
//

#if !os(macOS)
import UIKit

// Based entirely on the 'Fattie' solution from: https://stackoverflow.com/questions/25471114/how-to-validate-an-e-mail-address-in-swift

/*
Justification:

 1 - it avoids the many terrible regex mistakes you often see in these suggestions

 2 - it does NOT allow stupid emails such as "x@x" which are thought to be valid under certain RFCs, but are completely silly, can't be used as emails, and which your support staff would reject instantly, and which all mailer services (mailchimp, google, aws, etc) simply reject. If (for some reason) you need a solution that allows strings such as 'x@x', use another solution.

 3 - the code is very, very, very understandable

 4 - it is KISS, reliable, and tested to destruction on commercial apps with enormous numbers of users

 5 - a technical point, the predicate is a global, as Apple says it should be (watch out for code suggestions which don't have this)
 */

let __firstpart = "[A-Z0-9a-z]([A-Z0-9a-z._%+-]{0,30}[A-Z0-9a-z])?"
let __serverpart = "([A-Z0-9a-z]([A-Z0-9a-z-]{0,30}[A-Z0-9a-z])?\\.){1,5}"
let __emailRegex = __firstpart + "@" + __serverpart + "[A-Za-z]{2,40}"
let __emailPredicate = NSPredicate(format: "SELF MATCHES %@", __emailRegex)

/*
Explanation:

In the following description, "OC" means ordinary character - a letter or a digit.

__firstpart ... has to start and end with an OC. For the characters in the middle you can have certain characters such as underscore, but the start and end have to be an OC. (However, it's ok to have only one OC and that's it, for example: j@blah.com)

__serverpart ... You have sections like "blah." which repeat. (So mail.city.fcu.edu type of thing.) The sections have to start and end with an OC, but in the middle you can also have a dash "-". (If you want to allow other unusual characters in there, perhaps the underscore, simply add before the dash.) It's OK to have a section which is just one OC. (As in joe@w.campus.edu) You can have up to five sections, you have to have one. Finally the TLD (such as .com) is strictly 2 to 8 in size . Obviously, just change that "8" as preferred by your support department.
*/

public extension String {
    func isEmail() -> Bool {
        return __emailPredicate.evaluate(with: self)
    }
}

public extension UITextField {
    func isEmail() -> Bool {
        return self.text!.isEmail()
    }
}


public class FieldValidation {
    class func isValidEmail(_ email: String) -> Bool {
        return email.isEmail()
    }
}
#endif

