//
//  UILabel+Extension.swift
//  
//
//  Created by Chris Scalcucci on 1/21/21.
//

#if !os(macOS)
import UIKit

public extension UILabel {
    func setFontSize(_ size: CGFloat) {
        self.font =  UIFont(name: self.font.fontName, size: size)
        //self.sizeToFit()
    }
}

public extension UITextView {
    func setFontSize(_ size: CGFloat) {
        self.font = UIFont(name: self.font?.fontName ?? "", size: size)
        //self.sizeToFit()
    }
}
#endif
