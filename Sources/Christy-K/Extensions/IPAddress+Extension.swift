//
//  File.swift
//  
//
//  Created by Chris Scalcucci on 1/18/21.
//

import Foundation
import Network

@available(OSX 10.14, *)
public extension IPAddress {
    var isV6 : Bool {
        return self is IPv6Address
    }

    var isV4 : Bool {
        return self is IPv4Address
    }

    var host : String? {
        if let v6Addr = self as? IPv6Address {
            return v6Addr.debugDescription
        } else if let v4Addr = self as? IPv4Address {
            return v4Addr.debugDescription
        }
        return nil
    }
}

@available(OSX 10.14, *)
public extension IPv4Address {

    /**
         Utility routine to check if the Interface address is a
         site local address.

         - returns: True if the address is site local, false if
             address is note a site local unicase address
      */
    var isSiteLocalAddress : Bool {
        let bytes = [UInt8](self.rawValue)
        return
            (bytes[0] == 10 ||
            (bytes[0] == 172 && bytes[1] == 16) ||
            (bytes[0] == 192 && bytes[1] == 168))
    }
}

@available(OSX 10.14, *)
public extension IPv6Address {

    /**
         Utility routine to check if the Interface address is a
         site local address.

         - returns: True if the address is site local, false if
             address is note a site local unicase address
      */
    var isSiteLocalAddress : Bool {
        let bytes = [UInt8](self.rawValue)
        return ((bytes[0] & 0xff) == 0xfe
             && (bytes[1] & 0xc0) == 0x80)
    }
}



/*
 /**
      Utility routine to check if the Interface address is a
      site local address.

      - returns: True if the address is site local, false if
          address is note a site local unicase address
   */
  public var isSiteLocalAddress : Bool {
      guard let bytes = self.addressBytes else { return false }
      switch self.family {
      case .ipv4:
          return
              (bytes[0] == 10 ||
              (bytes[0] == 172 && bytes[1] == 16) ||
              (bytes[0] == 192 && bytes[1] == 168))
      case .ipv6:
          return ((bytes[0] & 0xff) == 0xfe
               && (bytes[1] & 0xc0) == 0x80)
      default:
          return false
      }
  }

 */

/*
 import NetUtils
 import Network

 public extension Interface {
     /**
          Utility routine to check if the Interface address is a
          site local address.

          - returns: True if the address is site local, false if
              address is note a site local unicase address
       */
     var isSiteLocalAddress : Bool {
          guard let bytes = self.addressBytes else { return false }
          switch self.family {
          case .ipv4:
              return
                  (bytes[0] == 10 ||
                  (bytes[0] == 172 && bytes[1] == 16) ||
                  (bytes[0] == 192 && bytes[1] == 168))
          case .ipv6:
              return ((bytes[0] & 0xff) == 0xfe
                   && (bytes[1] & 0xc0) == 0x80)
          default:
              return false
          }
      }
 }

 extension Interface {
     /**
         Calls the static allInterfaces() method and filters down
         to return the Interface that matches the given address.
      */
     public class func getByIPAddress(_ addr: String) -> Interface? {
         return Interface.allInterfaces().filter({ $0.address == addr}).first
     }

     /**
         Returns an array of IPAddresses associated with the given address string.

         For example: If you pass in an IPv4 address, it will find the Interface
         it belongs to and return both of that Interface's associated IPs.
      */
     public class func getAssociatedAddresses(_ addr: String) -> [IPAddress] {
         let allInterfaces = Interface.allInterfaces()
         guard let chosen = allInterfaces.filter({ $0.address == addr }).first else {
             return []
         }
         return allInterfaces
             .filter({ $0.name == chosen.name })
             .compactMap({ i in
                 return i.getIPAddress()
             })
     }

     public class func V6fromV4(_ addr: String) -> IPAddress? {
         return inverse(addr, .ipv6) as? IPv6Address
     }

     public class func V4fromV6(_ addr: String) -> IPAddress? {
         return inverse(addr, .ipv4)
     }

     private class func inverse(_ addr: String, _ family: Interface.Family) -> IPAddress? {
         let allInterfaces = Interface.allInterfaces()
         guard let chosen = allInterfaces.filter({ $0.address == addr }).first else {
             return nil
         }
         guard let inverted = allInterfaces
             .filter({ $0.name == chosen.name && $0.family == family }).first else {
                 return nil
         }
         return inverted.getIPAddress()
     }

     public func getIPAddress() -> IPAddress? {
         if let bytes = addressBytes {
             switch self.family {
             case .ipv4:
                 return IPv4Address(bytes.data())
             default:
                 return IPv6Address(bytes.data())
             }
         }
         if let addr = address {
             switch self.family {
             case .ipv4:
                 return IPv4Address(addr)
             default:
                 return IPv6Address(addr)
             }
         }
         return nil
     }

     /**
         Returns all IPAddresses associated with the given Interface.

         This first calls the static allInterfaces() method and filters.
      */
     public func getIPAddresses() -> [IPAddress] {
         // Attempts to create an IPv4 or IPv6 address from
         return Interface.allInterfaces()
             .filter({ $0.name == self.name })
             .compactMap({ i in
                 return i.getIPAddress()
             })
      }
 }

 */
