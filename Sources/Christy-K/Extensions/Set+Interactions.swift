//
//  Set+Extension.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 5/5/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public extension Set {

    mutating func adding(_ e: Element) -> Set<Element> {
        self.update(with: e)

        return self
    }

    mutating func removing(_ e: Element) -> Set<Element> {
        self.remove(e)
        return self
    }

    mutating func updating(_ e: Element) -> Set<Element> {
        self.update(with: e)
        return self
    }
}
