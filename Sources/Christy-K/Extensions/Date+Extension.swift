//
//  Date+Extension.swift
//  whitestar-swift
//
//  Created by Chris Scalcucci on 12/9/19.
//  Copyright © 2019 Society. All rights reserved.
//

import Foundation

public extension Date {
    /// Returns the current time in milliseconds from target date.
    /// Default is 1970.
    ///
    /// - parameter since: Date to pull milliseconds from
    /// - returns: Time in milliseconds from target date.
    static func currentTimeMillis(_ since: Date? = nil) -> Int64 {
        return (since != nil) ?
            Int64(Date().timeIntervalSince(since!) * 1000) :
        Date.currentSystemTimeMillis()
    }

    /// Returns the current time in milliseconds from target date.
    /// Default is 1970.
    ///
    /// - parameter since: Date to pull milliseconds from
    /// - returns: Time in milliseconds from target date.
    func currentTimeMillis(_ since: Date? = nil) -> Int64 {
        return Date.currentTimeMillis(since)
    }

    static func currentSystemTimeMillis() -> Int64 {
        var darwinTime : timeval = timeval(tv_sec: 0, tv_usec: 0)
        gettimeofday(&darwinTime, nil)
        return (Int64(darwinTime.tv_sec) * 1000) + Int64(darwinTime.tv_usec / 1000)
    }
}
