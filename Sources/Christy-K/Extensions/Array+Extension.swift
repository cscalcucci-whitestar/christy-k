//
//  Array+Extension.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 5/5/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public extension Array {
    mutating func appending(_ e: Element) -> [Element] {
        self.append(e)
        return self
    }
}

public extension Array where Element : AnyObject {
    mutating func appendingUnique(_ e: Element) -> [Element] {
        var unique: Bool = true

        var i = 0

        while i < self.count {
            if self[i] === e {
                unique = false
                // Break out
                i = self.count
            } else {
                i += 1
            }
        }

        return unique ? self.appending(e) : self
    }
}

public extension Array where Element : Equatable {
    mutating func removeFirst(where fn: (Element) -> Bool) {
        var i = 0

        while i < self.count {
            if fn(self[i]) {
                self.remove(at: i)
                return 
            }
            i += 1 
        }
    }

    mutating func appendingUnique(_ e: Element) -> [Element] {
        var unique: Bool = true

        var i = 0

        while i < self.count {
            if self[i] == e {
                unique = false
                // Break out
                i = self.count
            } else {
                i += 1
            }
        }

        return unique ? self.appending(e) : self
    }
}

public extension Array where Element : AnyObject & Equatable {
    mutating func appendingUnique(_ e: Element) -> [Element] {
        var unique: Bool = true

        var i = 0

        while i < self.count {
            let x = self[i]
            if x === e || x == e {
                unique = false
                // Break out
                i = self.count
            } else {
                i += 1
            }
        }

        return unique ? self.appending(e) : self
    }
}
