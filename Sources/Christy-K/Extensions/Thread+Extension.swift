//
//  File.swift
//  
//
//  Created by Chris Scalcucci on 1/18/21.
//

import Foundation

public extension Thread {
    @available(OSX 10.12, *)
    convenience init(_ name: String, _ priority: Double? = nil, _ block: @escaping () -> Void) {
        self.init(block: block)
        self.name = name

        if let priority = priority {
            self.threadPriority = priority
        }
    }

    @available(OSX 10.12, *)
    convenience init(_ name: String, _ block: @escaping () -> Void) {
        self.init(block: block)
        self.name = name
    }
}
