//
//  Double+Extension.swift
//  whitestar-swift
//
//  Created by Chris Scalcucci on 12/4/19.
//  Copyright © 2019 Society. All rights reserved.
//

import Foundation

public extension Double {
    func int() -> Int {
        return Int(self)
    }
}
