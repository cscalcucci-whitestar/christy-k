//
//  Character+Extension.swift
//  Christy-K
//
//  Created by Chris Scalcucci on 1/18/21.
//

import Foundation

public extension Int {
    static func char(_ char: Character) -> Int? {
        return Int(char.unicodeScalars[char.unicodeScalars.startIndex].value)
    }
}
public extension Character {
    func int() -> Int {
        return Int.char(self)!
    }
}
public func -(left: Character, right: Character) -> Int {
    return left.int() - right.int()
}
public func +(left: Character, right: Character) -> Int {
    return left.int() + right.int()
}
public func +=(left: inout String, right: Int) {
    left += "\(UnicodeScalar(right)!)"
}
public func +=(left: inout Character, right: Int) {
    left = left + right
}

public func +(left: Character, right: Int) -> Character {
    return Character(UnicodeScalar(Int(left.unicodeScalars[left.unicodeScalars.startIndex].value) + right) ?? left.unicodeScalars[left.unicodeScalars.startIndex])
}
public func -=(left: inout Character, right: Int) {
    left = left - right
}
public func -(left: Character, right: Int) -> Character {
    return Character(UnicodeScalar(Int(left.unicodeScalars[left.unicodeScalars.startIndex].value) - right) ?? left.unicodeScalars[left.unicodeScalars.startIndex])
}
public func >(left: Character, right: Int) -> Bool {
    return left.int() > right
}
public func >=(left: Character, right: Int) -> Bool {
    return left.int() >= right
}
public func <(left: Character, right: Int) -> Bool {
    return left.int() < right
}
public func <=(left: Character, right: Int) -> Bool {
    return left.int() <= right
}
