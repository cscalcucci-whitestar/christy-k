//
//  Range+Extension.swift
//  WSDemoApp
//
//  Created by Aaron on 6/5/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public extension Range where Range.Bound == String.Index {

    /**
        The safe way to extract an Int index from a range.
     */
    func start<T: StringProtocol>(in str: T) -> Int {
        return self.lowerBound.utf16Offset(in: str)
    }
    /**
        The safe way to extract an Int index from a range.
     */
    func end<T: StringProtocol>(in str: T) -> Int {
        return self.upperBound.utf16Offset(in: str)
    }

    /**
        USE AT YOUR OWN RISK.

        This is deprecated in favor of the utf16Offset to
        address grapheme clusters.
     */
    func start() -> Int {
        return self.lowerBound.encodedOffset
    }
    /**
        USE AT YOUR OWN RISK.

        This is deprecated in favor of the utf16Offset to
        address grapheme clusters.
     */
    func end() -> Int {
        return self.upperBound.encodedOffset
    }

}
