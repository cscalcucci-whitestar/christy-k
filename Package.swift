// swift-tools-version:5.4
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ChristyK",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "ChristyK",
            targets: ["Christy-K"]),
    ],
    dependencies: [
        .package(name: "Gzip",
                 url: "https://github.com/1024jp/GzipSwift.git",
                 from: "5.1.1"),
        .package(url: "https://github.com/uber/swift-concurrency.git", from: "0.7.1")
    ],
    targets: [
        /// The ObjCXX Library
        .target(
            name: "ChristyKObjCXX",
            exclude: ["include"] // will be fixed with PR-2814
        ),
        .target(
            name: "Christy-K",
            dependencies: ["ChristyKObjCXX", "Gzip"]),
        .testTarget(
            name: "Christy-KTests",
            dependencies: ["Christy-K"]),
    ],
    cxxLanguageStandard: .cxx17
)
